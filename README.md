# 工程简介

z-blog，我的个人博客

# 延伸阅读

## 开发规约

- 不允许接口返回值为String类型，返回String值时需要使用Result进行包装

## 缩写约定

- RO，Result Object，结果对象，用于封装某些服务的调用结果
- RPO，Request Parameters Object，请求参数对象，用于封装复杂的（>=3）请求参数
- DO，Data Object，数据对象，字段一一对应表的字段，用于储存从数据库查询到的最原始的数据
- DAO，Data Access Object，数据访问对象，用于数据库的增删改查，对应应用中的Mapper接口
- DTO，Data Transfer Object，数据传输对象，用于封装各种数据返回
- PO，Param Object，参数对象