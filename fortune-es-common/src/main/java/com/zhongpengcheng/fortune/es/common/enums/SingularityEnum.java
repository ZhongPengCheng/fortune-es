package com.zhongpengcheng.fortune.es.common.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 17:05:00
 */
@Getter
public enum SingularityEnum implements Mappable<SingularityEnum> {
    TIDAL_LOCKED(1, "潮汐锁定永昼永夜"),
    TIDAL_LOCKED_2(2, "潮汐锁定1:2"),
    TIDAL_LOCKED_4(4, "潮汐锁定1:4"),
    LAY_SIDE(8, "横躺自转"),
    CLOCKWISE_ROTATE(16, "反向自转"),
    MULTIPLE_SATELLITES(32, "多卫星");

    private final Integer id;
    private final String name;
    private static Map<Integer, SingularityEnum> enumMap;

    SingularityEnum(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public SingularityEnum getById(Integer id) {
        if (enumMap == null) {
            enumMap = new HashMap<>(30);
            for (SingularityEnum singularityEnum : SingularityEnum.values()) {
                enumMap.put(singularityEnum.getId(), singularityEnum);
            }
        }
        return enumMap.get(id);
    }
}
