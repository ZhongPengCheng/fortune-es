package com.zhongpengcheng.fortune.es.common.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 16:24:00
 */
@Getter
public enum ThemeEnum implements Mappable<ThemeEnum> {
    OCEAN_1(1, "地中海"),
    GAS_1(2, "气态巨星"),
    GAS_2(3, "气态巨星"),
    GAS_3(4, "冰巨星"),
    GAS_4(5, "冰巨星"),
    DESERT_1(6, "干旱荒漠"),
    DESERT_2(7, "灰烬冻土"),
    OCEAN_2(8, "海洋丛林"),
    LAVA_1(9, "熔岩"),
    ICE_1(10, "冰原冻土"),
    DESERT_3(11, "贫瘠荒漠"),
    DESERT_4(12, "戈壁"),
    VOLCANIC_1(13, "火山灰"),
    OCEAN_3(14, "红石"),
    OCEAN_4(15, "草原"),
    OCEAN_5(16, "水世界"),
    DESERT_5(17, "红土荒漠"),
    OCEAN_6(18, "白色海洋"),
    DESERT_6(19, "三色荒漠"),
    DESERT_7(20, "红冰荒漠");

    private final Integer id;
    private final String name;
    private static Map<Integer, ThemeEnum> enumMap;

    ThemeEnum(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public ThemeEnum getById(Integer id) {
        if (enumMap == null) {
            enumMap = new HashMap<>(30);
            for (ThemeEnum themeEnum : ThemeEnum.values()) {
                enumMap.put(themeEnum.getId(), themeEnum);
            }
        }
        return enumMap.get(id);
    }
}
