package com.zhongpengcheng.fortune.es.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * @author zhongpengcheng
 * @date 2021-10-01 18:01:24
 **/
public enum DeleteEnum {
    /**
     * 删除
     */
    DELETED(1),
    /**
     * 正常
     */
    NOT_DELETED(0);

    @EnumValue
    private final int value;

    DeleteEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
