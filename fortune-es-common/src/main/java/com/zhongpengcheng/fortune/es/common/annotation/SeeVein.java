package com.zhongpengcheng.fortune.es.common.annotation;


import com.zhongpengcheng.fortune.es.common.enums.VeinEnum;

import java.lang.annotation.*;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 16:55:00
 */
@Inherited
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface SeeVein {
    VeinEnum value();
}
