package com.zhongpengcheng.fortune.es.common.pojo.po;

import lombok.Data;
import org.springframework.data.domain.PageRequest;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-10 15:44:00
 */
@Data
public class Page {

    public static final Page DEFAULT = new Page();

    private Integer current = 1;
    private Integer size = 10;
    private Integer total;

    public Integer from() {
        return current - 1;
    }

    public PageRequest pageRequest() {
        return PageRequest.of(from(), size);
    }
}
