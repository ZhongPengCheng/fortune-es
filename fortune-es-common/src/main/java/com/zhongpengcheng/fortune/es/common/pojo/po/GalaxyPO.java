package com.zhongpengcheng.fortune.es.common.pojo.po;

import lombok.Data;

/**
 * Galaxy Query Object
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 13:06:00
 */
@Data
public class GalaxyPO {
    /**
     * 星系数
     */
    private Integer starCount = 64;
    /**
     * 星系的星球数量
     */
    private Integer planetCount;
    /**
     * 星系主星类型
     */
    private Integer starType;
    /**
     * 星系与初始星系的最远距离，单位光年
     */
    private Integer distance = 1000;
    /**
     * 星系的最小光度
     */
    private Double dysonLumino;
    /**
     * 特性筛选条件（潮汐锁定永昼永夜）
     */
    private SingularityPO singularityQO;
    /**
     * 星球类型筛选条件
     */
    private ThemePO themeQO;
    /**
     * 矿物筛选条件（包含水、原油及硫酸）
     */
    private VeinPO veinQO;
    /**
     * 分页参数
     */
    private Page page = Page.DEFAULT;
}




