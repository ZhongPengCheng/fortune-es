package com.zhongpengcheng.fortune.es.common.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 16:44:00
 */
@Getter
public enum VeinEnum implements Mappable<VeinEnum> {
    FE(1, "铁矿脉"),
    CU(2, "铜矿脉"),
    SI(3, "硅矿脉"),
    TI(4, "钛矿脉"),
    STONE(5, "石矿脉"),
    COAL(6, "煤矿脉"),
    OIL(7, "原油涌泉"),
    FIRE_ICE(8, "可燃冰矿"),
    DIAMOND(9, "金伯利矿"),
    FRACTAL(10, "分形硅矿"),
    CRYSRUB(11, "有机晶体矿"),
    GRAT(12, "光栅石矿"),
    BAMBOO(13, "刺笋矿脉"),
    MAG(14, "单极磁矿"),
    WATER(1000, "海洋"),
    SULPHURIC(1116, "硫酸海洋");

    private final Integer id;
    private final String name;
    private static Map<Integer, VeinEnum> enumMap;

    VeinEnum(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public VeinEnum getById(Integer id) {
        if (enumMap == null) {
            enumMap = new HashMap<>(30);
            for (VeinEnum veinEnum : VeinEnum.values()) {
                enumMap.put(veinEnum.getId(), veinEnum);
            }
        }
        return enumMap.get(id);
    }
}
