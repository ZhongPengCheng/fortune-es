package com.zhongpengcheng.fortune.es.common.enums;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 16:58:00
 */
public interface Mappable<T> {
    T getById(Integer id);
}
