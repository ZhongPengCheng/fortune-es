package com.zhongpengcheng.fortune.es.common.enums;

/**
 * 接口状态码枚举类
 * @author zhongpengcheng
 */
public enum ResultCodeEnum {
    /**
     * 操作成功
     */
    SUCCESS("一切ok", "00000"),
    /**
     * 操作失败，用户错误
     */
    FAILED("用户端错误", "A0001"),
    /**
     * 操作失败，系统错误
     */
    ERROR("系统执行出错", "B0001"),
    USERNAME_HAS_EXISTS("用户名已存在", "A0111"),
    USER_LOGIN_ERROR("用户登录异常", "A0200"),
    USER_ACCOUNT_DOES_NOT_EXIST("用户账户不存在" , "A0201"),
    USER_NOT_EXIST("用户不存在" , "A0202"),
    USER_PASSWORD_ERROR("用户名或密码错误", "A0210"),
    INVALID_ACCESS_CREDENTIALS("无效的访问凭证", "A0231"),
    INVALID_REFRESH_CREDENTIALS("无效的刷新凭证", "A0232"),
    CAPTCHA_HAS_EXPIRED("验证码已过期", "A0241"),
    ACCESS_NOT_AUTHORIZED("访问未授权", "A0301"),
    USER_REQUEST_PARAMETER_ERROR("用户请求参数错误", "A0400"),
    REQUEST_REQUIRED_PARAMETER_IS_NULL("请求必填参数为空", "A0410"),
    API_DOES_NOT_EXIST("API不存在", "A0411");

    private final String msg;
    private final String code;

    ResultCodeEnum(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public String getCode() {
        return code;
    }
}
