package com.zhongpengcheng.fortune.es.common.annotation;

import com.zhongpengcheng.fortune.es.common.enums.SingularityEnum;

import java.lang.annotation.*;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 17:10:00
 */
@Inherited
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface SeeSingularity {
    SingularityEnum value();
}
