package com.zhongpengcheng.fortune.es.common.pojo.po;

import com.zhongpengcheng.fortune.es.common.annotation.SeeSingularity;
import lombok.Data;

import static com.zhongpengcheng.fortune.es.common.enums.SingularityEnum.*;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-10 16:55:00
 */
@Data
public class SingularityPO {
    @SeeSingularity(TIDAL_LOCKED)
    private Integer tidalLocked;
    @SeeSingularity(TIDAL_LOCKED_2)
    private Integer tidalLocked2;
    @SeeSingularity(TIDAL_LOCKED_4)
    private Integer tidalLocked4;
    @SeeSingularity(LAY_SIDE)
    private Integer laySide;
    @SeeSingularity(CLOCKWISE_ROTATE)
    private Integer clockwiseRotate;
    @SeeSingularity(MULTIPLE_SATELLITES)
    private Integer multipleSatellites;
}
