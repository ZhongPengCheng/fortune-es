package com.zhongpengcheng.fortune.es.common.pojo;

import cn.hutool.core.date.DateUtil;
import com.zhongpengcheng.fortune.es.common.enums.ResultCodeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Optional;

/**
 * Web返回结果封装
 * @author zhongpengcheng
 */
@Getter
@Setter
public class Result<T> implements Serializable {
    private static final long serialVersionUID = -3887725517645851696L;
    /**
     * 返回数据
     */
    private T data;
    /**
     * 响应码
     */
    private String code;
    /**
     * 响应消息
     */
    private String msg;
    /**
     * 状态描述文本
     */
    private String statusText;
    /**
     * 操作时间（yyyy-MM-dd HH:mm:ss格式，s），默认为当前时间
     */
    @Builder.Default
    private String serverTime = DateUtil.now();

    public Result(ResultCodeEnum resultCode, T data) {
        this(resultCode, resultCode.getMsg(), data);
    }


    public Result(ResultCodeEnum resultCode, String statusText, T data) {
        this.code = resultCode.getCode();
        this.msg = resultCode.getMsg();
        this.statusText = Optional.ofNullable(statusText).orElse(resultCode.getMsg());
        this.data = data;
    }

    public Boolean isSuccess() {
        return ResultCodeEnum.SUCCESS.getCode().equals(code);
    }

    @SuppressWarnings("unused")
    public Boolean isFailed() {
        return !isSuccess();
    }
}
