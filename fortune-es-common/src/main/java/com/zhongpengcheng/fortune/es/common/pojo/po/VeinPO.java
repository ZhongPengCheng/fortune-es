package com.zhongpengcheng.fortune.es.common.pojo.po;

import com.zhongpengcheng.fortune.es.common.annotation.SeeVein;
import lombok.Data;

import static com.zhongpengcheng.fortune.es.common.enums.VeinEnum.*;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-10 16:56:00
 */
@Data
public class VeinPO {
    @SeeVein(FE)
    private Boolean fe;
    @SeeVein(CU)
    private Boolean cu;
    @SeeVein(SI)
    private Boolean si;
    @SeeVein(TI)
    private Boolean ti;
    @SeeVein(STONE)
    private Boolean stone;
    @SeeVein(COAL)
    private Boolean coal;
    @SeeVein(OIL)
    private Boolean oil;
    @SeeVein(FIRE_ICE)
    private Boolean fireIce;
    @SeeVein(DIAMOND)
    private Boolean diamond;
    @SeeVein(FRACTAL)
    private Boolean fractal;
    @SeeVein(CRYSRUB)
    private Boolean crysrub;
    @SeeVein(GRAT)
    private Boolean grat;
    @SeeVein(BAMBOO)
    private Boolean bamboo;
    @SeeVein(MAG)
    private Boolean mag;
    @SeeVein(WATER)
    private Boolean water;
    @SeeVein(SULPHURIC)
    private Boolean sulphuric;
}
