package com.zhongpengcheng.fortune.es.common.annotation;

import java.lang.annotation.*;

/**
 * 检查ES密钥权限
 *
 * @author zhongpengcheng
 * @date 2021-10-01 21:14:31
 **/
@Documented
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface CheckESecret {
    String ES_SECRET_HEADER = "Fortune ES";
}
