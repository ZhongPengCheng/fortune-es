package com.zhongpengcheng.fortune.es.service.impl;

import com.alibaba.fastjson.JSON;
import com.zhongpengcheng.fortune.es.FortuneElasticSearchApplication;
import com.zhongpengcheng.fortune.es.common.pojo.po.GalaxyPO;
import com.zhongpengcheng.fortune.es.common.pojo.po.Page;
import com.zhongpengcheng.fortune.es.common.pojo.po.SingularityPO;
import com.zhongpengcheng.fortune.es.common.pojo.po.VeinPO;
import com.zhongpengcheng.fortune.es.pojo.dto.GalaxyDTO;
import com.zhongpengcheng.fortune.es.service.SeedSearchService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(args = {"--mpw.key=29147c649b57648b"},classes = FortuneElasticSearchApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StarQueryBuilderTest {
    GalaxyPO find4TidalLocked;
    StarQueryBuilder starQueryBuilder;

    @Autowired
    SeedSearchService searchService;

    @BeforeEach
    void setUp() {
        find4TidalLocked = new GalaxyPO();


        SingularityPO singularityQO = new SingularityPO();
        singularityQO.setTidalLocked(4);

        VeinPO veinQO = new VeinPO();
        veinQO.setFe(true);
        veinQO.setCu(true);
        veinQO.setSi(true);
        veinQO.setStone(true);
        veinQO.setTi(true);

        Page page = new Page();
        page.setCurrent(1);
        page.setSize(10);

        find4TidalLocked.setDysonLumino(1.5);
        find4TidalLocked.setSingularityQO(singularityQO);
        find4TidalLocked.setVeinQO(veinQO);

        starQueryBuilder = StarQueryBuilder.of(find4TidalLocked);
    }

    @Test
    void testGenQuery() {
        System.out.println(starQueryBuilder.toQuery());
        ArrayList<GalaxyDTO> galaxyList = searchService.searchSeed(find4TidalLocked);
        assertNotNull(galaxyList);
        assertTrue(galaxyList.size() > 0);
        System.out.println(JSON.toJSONString(galaxyList, true));
    }
}