package com.zhongpengcheng.fortune.es.advice;

import com.zhongpengcheng.fortune.es.common.pojo.Result;
import com.zhongpengcheng.fortune.es.util.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常拦截
 *
 * @author zhongpengcheng
 * @date 2021-10-02 20:40:33
 **/
@RestControllerAdvice(annotations = RestController.class)
@Slf4j
public class GlobalExceptionAdvice {

    @ExceptionHandler
    public Result<String> globalExceptionHandler(Exception e) {
        log.error("", e);
        return ResultUtils.failed(e.getMessage());
    }
}
