package com.zhongpengcheng.fortune.es.config;

import com.zhongpengcheng.fortune.es.prop.ESPermissionProperties;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * ES配置类
 *
 * @author zhongpengcheng
 * @date 2021-10-02 20:12:08
 **/
@Configuration
@Slf4j
@EnableConfigurationProperties({ESPermissionProperties.class})
public class ESConfiguration {
    @Getter
    private ESPermissionProperties esProperty;

    @Autowired
    public void setEsProperty(ESPermissionProperties esProperty) {
        this.esProperty = esProperty;
    }
}
