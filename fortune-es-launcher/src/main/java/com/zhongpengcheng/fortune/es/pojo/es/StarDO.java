package com.zhongpengcheng.fortune.es.pojo.es;

import lombok.Data;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 14:28:00
 */
@Data
public class StarDO {
    private Boolean birth;
    private Integer distance;
    private Integer dysonLumino;
    private Integer type;
    private Integer id;
    private String name;
    private Integer dysonContain;
    private VeinDO veins;
    private SingularityDO singularities;
    private ThemeDO themes;
}
