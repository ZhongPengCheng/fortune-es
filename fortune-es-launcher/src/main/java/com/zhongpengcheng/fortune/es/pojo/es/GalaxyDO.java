package com.zhongpengcheng.fortune.es.pojo.es;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

/**
 * Document注解可以指定要查询的索引
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 14:27:00
 */
@Data
@Document(indexName = "fortune_galaxy_data_simple")
public class GalaxyDO {
    private Integer birthPlanetId;
    private List<StarDO> stars;
}
