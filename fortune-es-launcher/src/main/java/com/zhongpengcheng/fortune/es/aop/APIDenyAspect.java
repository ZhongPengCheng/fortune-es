package com.zhongpengcheng.fortune.es.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * API访问黑名单拦截
 *
 * @author zhongpengcheng
 * @date 2021-10-01 20:28:31
 **/
@Component
@Slf4j
@Aspect
public class APIDenyAspect {

    /**
     * 拦截公开的接口
     */
    @Pointcut("execution(public * com.zhongpengcheng.fortune.es.controller..*.*(..))")
    public void api() {}
}
