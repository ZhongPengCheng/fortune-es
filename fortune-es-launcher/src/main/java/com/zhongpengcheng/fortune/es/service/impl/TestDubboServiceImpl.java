package com.zhongpengcheng.fortune.es.service.impl;

import cn.hutool.core.date.DateUtil;
import com.zhongpengcheng.fortune.es.service.TestDubboService;
import org.springframework.stereotype.Component;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-12 21:11:00
 */
@Component
public class TestDubboServiceImpl implements TestDubboService {
    @Override
    public String sayHelloToConsumer(String name) {
        return "Hello " + name + ", now is " + DateUtil.now();
    }
}
