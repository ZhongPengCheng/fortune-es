package com.zhongpengcheng.fortune.es.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhongpengcheng.fortune.es.pojo.db.APIDenyDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 黑名单Mapper
 * @author zhongpengcheng
 * @date 2021-10-01 18:05:46
 **/
@Mapper
public interface APIDenyMapper extends BaseMapper<APIDenyDO> {
}
