package com.zhongpengcheng.fortune.es.service;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-12 21:10:00
 */
public interface TestDubboService {
    String sayHelloToConsumer(String name);
}
