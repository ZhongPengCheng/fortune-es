package com.zhongpengcheng.fortune.es.config;

import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;
import com.alibaba.csp.sentinel.init.InitFunc;
import com.zhongpengcheng.fortune.es.sentinel.PullModeLocalFileDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Sentinel配置
 *
 * @author zhongpengcheng
 * @date 2021-10-01 20:22:04
 **/
@Configuration
public class SentinelConfiguration {

    /**
     * 开启SentinelResource注解支持
     */
    @Bean
    public SentinelResourceAspect sentinelResourceAspect() {
        return new SentinelResourceAspect();
    }

    @Bean
    public InitFunc pullModeLocalFileDataSource(){
        return new PullModeLocalFileDataSource();
    }
}
