package com.zhongpengcheng.fortune.es.aop;

import com.zhongpengcheng.fortune.es.common.annotation.CheckESecret;
import com.zhongpengcheng.fortune.es.common.enums.ResultCodeEnum;
import com.zhongpengcheng.fortune.es.config.ESConfiguration;
import com.zhongpengcheng.fortune.es.util.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * ES敏感操作拦截
 * @author zhongpengcheng
 * @date 2021-10-01 21:15:27
 **/
@Aspect
@Component
@Slf4j
public class CheckESecretAspect {

    private String secret;

    @Pointcut("@annotation(com.zhongpengcheng.fortune.es.common.annotation.CheckESecret)")
    public void checkPoint() {
    }

    /**
     * 前置打印日志
     */
    @Before("checkPoint()")
    public void doBefore(JoinPoint joinPoint) {
        HttpServletRequest request = Objects.requireNonNull((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        log.info("Source IP={}, URL={}, HTTP Method={}, Class Method={}#{}", request.getRemoteAddr(),
                request.getRequestURL().toString(), request.getMethod(),
                joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
    }

    /**
     * 访问权限校验
     */
    @Around("checkPoint()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        if (!canAccess()) {
            return ResultUtils.failed(ResultCodeEnum.ACCESS_NOT_AUTHORIZED);
        }
        return proceedingJoinPoint.proceed();
    }

    /**
     * 验证访问者是否携带了正确的令牌
     * @return 正确返回true，否则返回false
     */
    private boolean canAccess() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        String secret = servletRequestAttributes.getRequest().getHeader(CheckESecret.ES_SECRET_HEADER);
        return this.secret.equals(secret);
    }

    @Autowired
    public void setEsConfiguration(ESConfiguration esConfiguration) {
        this.secret = esConfiguration.getEsProperty().getSecret();
        Assert.notNull(secret, "没有ES操作权限密钥，启动失败");
    }
}
