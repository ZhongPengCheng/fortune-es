package com.zhongpengcheng.fortune.es.facade.impl;

import com.zhongpengcheng.fortune.es.facade.FortuneElasticSearchFacade;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-13 21:03:00
 */
@DubboService(version = "1.0")
@Service
public class FortuneElasticSearchFacadeImpl implements FortuneElasticSearchFacade {
}
