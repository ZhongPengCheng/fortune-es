package com.zhongpengcheng.fortune.es.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhongpengcheng.fortune.es.dao.APIDenyMapper;
import com.zhongpengcheng.fortune.es.pojo.db.APIDenyDO;
import com.zhongpengcheng.fortune.es.service.APIDenyService;
import org.springframework.stereotype.Service;

/**
 * @author zhongpengcheng
 * @date 2021-10-01 18:08:25
 **/
@Service
public class APIDenyServiceImpl extends ServiceImpl<APIDenyMapper, APIDenyDO> implements APIDenyService {
}
