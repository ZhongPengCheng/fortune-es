package com.zhongpengcheng.fortune.es.controller;

import com.zhongpengcheng.fortune.es.common.annotation.CheckESecret;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Fortune ES操作API
 *
 * @author zhongpengcheng
 * @date 2021-10-01 21:06:43
 **/
@RestController
public class ElasticSearchController {

    @PostMapping("/fortune/_bulk")
    @CheckESecret
    public Object bulkInsert() {
        return "321";
    }

    @GetMapping("/fortune/_mapping")
    @CheckESecret
    public Object getDefine() {
        return "123";
    }
}
