package com.zhongpengcheng.fortune.es.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhongpengcheng.fortune.es.pojo.db.APIDenyDO;

/**
 * 黑名单服务类
 *
 * @author zhongpengcheng
 * @date 2021-10-01 18:07:16
 **/
public interface APIDenyService extends IService<APIDenyDO> {
}
