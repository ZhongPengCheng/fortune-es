package com.zhongpengcheng.fortune.es.service;

import com.zhongpengcheng.fortune.es.common.pojo.po.GalaxyPO;
import com.zhongpengcheng.fortune.es.pojo.dto.GalaxyDTO;

import java.util.ArrayList;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-10 15:41:00
 */
public interface SeedSearchService {
    ArrayList<GalaxyDTO> searchSeed(GalaxyPO galaxyPO);
}
