package com.zhongpengcheng.fortune.es.pojo.dto;

import com.zhongpengcheng.fortune.es.pojo.es.StarDO;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-10 18:03:00
 */
@Data
public class GalaxyDTO {
    private Integer id;
    private Integer birthPlanetId;
    private List<StarDO> stars;
}
