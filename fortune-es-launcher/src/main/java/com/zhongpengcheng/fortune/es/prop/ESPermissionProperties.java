package com.zhongpengcheng.fortune.es.prop;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * ES配置项
 *
 * @author zhongpengcheng
 * @date 2021-10-02 20:10:06
 **/
@Getter
@Setter
@ConfigurationProperties("fortune.es.permission")
public class ESPermissionProperties {
    private String secret;
    private String responsePackage;
}
