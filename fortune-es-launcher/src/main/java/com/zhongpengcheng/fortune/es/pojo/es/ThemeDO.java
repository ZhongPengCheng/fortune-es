package com.zhongpengcheng.fortune.es.pojo.es;

import com.zhongpengcheng.fortune.es.common.annotation.SeeTheme;
import lombok.Data;

import static com.zhongpengcheng.fortune.es.common.enums.ThemeEnum.*;

/**
 * @author ZhongPengCheng
 * @version 1.0
 * @date 2021-10-07 14:30:00
 */
@Data
public class ThemeDO {
    @SeeTheme(ICE_1)
    private Boolean ice1;
    @SeeTheme(LAVA_1)
    private Boolean lava1;
    @SeeTheme(VOLCANIC_1)
    private Boolean volcanic1;
    @SeeTheme(GAS_1)
    private Boolean gas1;
    @SeeTheme(GAS_2)
    private Boolean gas2;
    @SeeTheme(GAS_3)
    private Boolean gas3;
    @SeeTheme(GAS_4)
    private Boolean gas4;
    @SeeTheme(OCEAN_1)
    private Boolean ocean;
    @SeeTheme(OCEAN_2)
    private Boolean ocean2;
    @SeeTheme(OCEAN_3)
    private Boolean ocean3;
    @SeeTheme(OCEAN_4)
    private Boolean ocean4;
    @SeeTheme(OCEAN_5)
    private Boolean ocean5;
    @SeeTheme(OCEAN_6)
    private Boolean ocean6;
    @SeeTheme(DESERT_1)
    private Boolean desert1;
    @SeeTheme(DESERT_2)
    private Boolean desert2;
    @SeeTheme(DESERT_3)
    private Boolean desert3;
    @SeeTheme(DESERT_4)
    private Boolean desert4;
    @SeeTheme(DESERT_5)
    private Boolean desert5;
    @SeeTheme(DESERT_6)
    private Boolean desert6;
    @SeeTheme(DESERT_7)
    private Boolean desert7;
}
