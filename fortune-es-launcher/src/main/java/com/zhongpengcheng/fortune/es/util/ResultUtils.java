package com.zhongpengcheng.fortune.es.util;

import com.zhongpengcheng.fortune.es.common.enums.ResultCodeEnum;
import com.zhongpengcheng.fortune.es.common.pojo.Result;
import org.springframework.lang.Nullable;

/**
 * 结果工具类
 *
 * @author zhongpengcheng
 * @date 2021-10-02 20:31:54
 **/
public class ResultUtils {
    private ResultUtils() {
    }

    /**
     * 成功结果
     * @param data 响应数据
     * @param <T> 响应数据泛型
     * @return 封装的成功响应结果对象
     */
    public static <T> Result<T> success(@Nullable T data) {
        return new Result<>(ResultCodeEnum.SUCCESS, data);
    }

    /**
     * 成功结果
     * @see ResultUtils#success(Object)
     */
    public static <T> Result<T> success() {
        return success(null);
    }

    /**
     * 失败结果
     * @param data 响应数据
     * @see ResultUtils#failed(ResultCodeEnum, String, Object)
     */
    public static <T> Result<T> failed(@Nullable T data) {
        return failed(ResultCodeEnum.FAILED, null, data);
    }

    /**
     * 失败结果
     * @see ResultUtils#failed(ResultCodeEnum, String, Object)
     */
    public static <T> Result<T> failed() {
        return failed(ResultCodeEnum.FAILED, null, null);
    }

    /**
     * 失败结果
     * @param resultCode 失败响应码
     * @see ResultUtils#failed(ResultCodeEnum, String, Object)
     */
    public static <T> Result<T> failed(ResultCodeEnum resultCode) {
        return failed(resultCode, null, null);
    }

    /**
     * 失败结果
     * @param resultCode 失败响应码
     * @param statusText 状态描述文本
     * @see ResultUtils#failed(ResultCodeEnum, String, Object)
     */
    public static <T> Result<T> failed(ResultCodeEnum resultCode, @Nullable String statusText) {
        return failed(resultCode, statusText, null);
    }

    /**
     * 失败结果
     * @param resultCode 失败状态枚举
     * @param statusText 状态描述文本
     * @param <T> 返回数据类型
     * @return 封装的失败响应结果对象
     */
    public static <T> Result<T> failed(ResultCodeEnum resultCode, @Nullable String statusText, @Nullable T data) {
        return new Result<>(resultCode, statusText, data);
    }
}
