package com.zhongpengcheng.fortune.es.sentinel;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhongpengcheng
 * @date 2021-10-08 16:10:42
 **/
public class PersistenceRuleConstant {

    /**
     * 存储文件路径
     */
    public static final String storePath = System.getProperty("user.home")+"\\sentinel\\rules\\";

    /**
     * 各种存储sentinel规则映射map
     */
    public static final Map<String,String> rulesMap = new HashMap<>();

    //流控规则文件
    public static final String FLOW_RULE_PATH = "flowRulePath";

    static {
        rulesMap.put(FLOW_RULE_PATH,storePath+"flowRule.json");
    }
}
