package com.zhongpengcheng.fortune.es;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhongpengcheng
 * @date 2021-10-01 17:22:33
 **/
@SpringBootApplication
@MapperScan(annotationClass = Mapper.class)
@EnableDubbo
public class FortuneElasticSearchApplication {
    public static void main(String[] args) {
        SpringApplication.run(FortuneElasticSearchApplication.class, args);
    }
}
