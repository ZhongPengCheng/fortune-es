package com.zhongpengcheng.fortune.es.util;

import com.zhongpengcheng.fortune.es.sentinel.PersistenceRuleConstant;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * @author zhongpengcheng
 * @date 2021-10-08 16:27:20
 **/
@Slf4j
public class RuleFileUtils {

    /**
     * 方法实现说明:若路径不存在就创建路径
     * @param filePath:文件存储路径
     */
    public static void mkdirIfNotExits(String filePath) {
        File file = new File(filePath);
        if(!file.exists()) {
            log.info("创建Sentinel规则目录:{}", filePath);
            boolean success = file.mkdirs();
            if (success) {
                log.info("创建Sentinel规则目录成功");
            } else {
                log.error("创建Sentinel规则目录失败");
            }
        }
    }


    /**
     * 方法实现说明:若文件不存在就创建路径
     * @param ruleFileMap 规则存储文件
     */
    public static void createFileIfNotExits(Map<String,String> ruleFileMap) throws IOException {

        Set<String> ruleFilePathSet = ruleFileMap.keySet();

        for (String ruleFilePathKey : ruleFilePathSet) {
            String ruleFilePath = PersistenceRuleConstant.rulesMap.get(ruleFilePathKey).toString();
            File ruleFile = new File(ruleFilePath);
            if (ruleFile.exists()) {
                log.info("创建Sentinel 规则文件:{}", ruleFile);
                ruleFile.createNewFile();
            }
        }
    }
}
