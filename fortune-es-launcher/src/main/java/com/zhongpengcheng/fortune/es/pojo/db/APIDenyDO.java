package com.zhongpengcheng.fortune.es.pojo.db;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhongpengcheng.fortune.es.common.enums.DeleteEnum;
import lombok.*;

import java.util.Date;

/**
 * 接口黑名单对象
 *
 * @author zhongpengcheng
 * @date 2021-10-01 17:59:57
 **/
@TableName("api_deny")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Builder
public class APIDenyDO {
    /**
     * 封禁的IP
     */
    private String ip;
    /**
     * 解锁时间
     */
    private Date unlockTime;
    /**
     * 封禁的url
     */
    private String url;
    /**
     * 主键id
     */
    @TableId("id")
    private Long id;
    /**
     * 是否被删除（0正常，1被删除）
     */
    @TableLogic
    private DeleteEnum isDeleted;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 操作人，默认system
     */
    private String operator;
}
